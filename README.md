This version of eddy is intended for use in the version 1.5 of the UK Biobank MRI image
processing pipeline (falmagro/UK_biobank_pipeline_v_1.5).

The eddy source code corresponds to eddy version 2006.0 (which was released as
part of FSL 6.0.2), with minor modifications to the Makefile to:

 - enable greater compatibility with CUDA hardware (additional gencode flags)
 - execution without the need for a CUDA toolkit to be installed (CUDA runtime
   statically linked)

The resulting binary should be copied into the $FSLDIR/bin/ directory of a
compatible FSL installation.

Compilation instructions are as follows, and assume that:
 - They are executed on a AlmaLinux 8 machine with a C++ compiler toolchain installed
 - FSL 6.0.2 is installed at `/fsl/`
 - The CUDA 11.0 Toolkit is installed at `/cuda/`.

```
docker run -it almalinux:8 /bin/bash

# install compiler toolchain and dependencies
dnf groupinstall -y "development tools"
dnf install -y libquadmath-devel

# Download and install FSL 6.0.2
wget https://fsl.fmrib.ox.ac.uk/fsldownloads/fsl-6.0.2-centos7_64.tar.gz
tar xf fsl-6.0.2-centos7_64.tar.gz

# Make builds go fast now
export MAKEFLAGS="-j 16"

# Configure FSL
export FSLDIR=$(pwd)/fsl
export PATH=$FSLDIR/bin:$PATH
source $FSLDIR/etc/fslconf/fsl.sh
source $FSLDIR/etc/fslconf/fsl-devel.sh

# Download and install the CUDA toolkit
wget https://developer.download.nvidia.com/compute/cuda/11.0.3/local_installers/cuda_11.0.3_450.51.06_linux.run
sh ./cuda_11.0.3_450.51.06_linux.run --silent --toolkit --installpath=/cuda/ --no-opengl-libs --no-drm


# Download project sources - the eddy Makefile pulls code from these source directories
git clone https://git.fmrib.ox.ac.uk/fsl/topup.git          && cd topup          && git checkout FSL6-0-2      && cd ..
git clone https://git.fmrib.ox.ac.uk/fsl/basisfield.git     && cd basisfield     && git checkout FSL6-0-0      && cd ..
git clone https://git.fmrib.ox.ac.uk/fsl/cudabasisfield.git && cd cudabasisfield && git checkout v1-0-0        && cd ..
git clone https://git.fmrib.ox.ac.uk/paulmc/eddy.git        && cd eddy           && git checkout ukbv1-rebuild && cd ..

# Build eddy - this will produce eddy_cuda11.0
cd eddy
make
```
